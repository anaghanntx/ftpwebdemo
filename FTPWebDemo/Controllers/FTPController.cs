﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C1.Web.Mvc;

namespace FTPWebDemo.Controllers
{
    public class FTPController : Controller
    {
        // GET: FTP
        public ActionResult Index()
        {
            var list = FTPWebDemo.Models.Folder.Create(Server.MapPath("~")).Children;
            return View(list);
        }
    }
}